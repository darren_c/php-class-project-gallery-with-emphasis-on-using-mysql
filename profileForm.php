<?php session_start(); ?>
<style type="text/css">
    .accountTable, .accountTable tr td
    {
        color: white;
        padding: .5em;
    }
    
    .accountTable tr td input[type=text],
    .accountTable tr td input[type=password],
    .accountTable tr td input[type=email],
    .accountTable tr td input[type=date], 
    .accountTable tr td textarea
    {
        border: solid 1px white;
        background: black;
        color: white;
        padding: 3px;
        margin-top: .5em;
        border-radius: 10px;
        margin-left: .5em;
        margin-right: .5em;
        transition: 500ms;
        text-indent: 10px;
    }
    
    .Btn
    {
        padding-left: 20px;
        padding-right: 20px;
        text-indent: 0px;
        padding: 4px;
        border: solid 1px white;
        background: transparent;
        color: white;
        width: 90px;
        cursor: pointer;
    }
    
    .Btn:hover
    {
        transition: 500ms;
        color: white;
    }
    
    textarea:focus, input:focus
    {
        outline: none;
        box-shadow: 0 0 10px white;
    }
</style>


<form action="scripts/changeProfile.php" method="POST" enctype="multipart/form-data">
    <table class="accountTable">
        <thead>
            <tr><td colspan="2"><h3>Profile Settings</h3></td></tr>
        </thead>
        
        <tbody>
            <tr>
                <td><label>Username</label></td>
                <td><input type="text" name="sUname" value="<?php echo $_SESSION['username']; ?>" /></td>
            </tr>   
            <tr>
                <td><label>Change Profile Picture</label></td>
                <td><input type="file" name="image" value="" size="50"/></td>
            </tr>
            <tr>
                <td><label>Date of Birth</label></td>
                <td><input type="date" name="dob" value="<?php echo $_SESSION['dob']; ?>" /></td>
            </tr>
            <tr>
                <td><label>Email</label></td>
                <td><input type="email" name="email" value="<?php echo $_SESSION['email']; ?>" /></td>
            </tr>
            <tr>
                <td><label>Bio</label></td>
                <td><textarea cols="30" rows="5" name="bio"></textarea></td>
            </tr>
            <tr>
                <td><label>Interests</label></td>
                <td><textarea cols="30" rows="5" name="Interests"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input class="Btn" type="submit" name="submit" />
                    <input class="Btn" type="reset" name="reset" />
                </td>
            </tr>
        </tbody>
    </table>
</form>

