# README #


#Setup
-This project requires phpMyAdmin login credentials to be placed under connection.php file.

-Included is a createdb.php script to initially setup the database.

#Summary

This project was created to display proficiency with PHP for our instructor. Our team used PHP dbo methods to access the database using WAMP server to host the database locally


#Status
Project is currently halted.

#Credit
Credits to my group members Deji Ojo and Andy Yang for creating this project with me.