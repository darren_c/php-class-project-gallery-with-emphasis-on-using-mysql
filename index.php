<?php
    //php code here
	require "connection.php";
	require 'core.php';
    require 'createdb.php';
	
	if(logged_in())
	{
		header('Location: account.php');
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to PicSwap</title>
        <link rel="icon" type="image/ico" href="css/logos/favicon.ico" />
        <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="css/index.css" />


    </head>
    
    <body>
        <div class="header" id="top">
			<div>
				<img src="css/logos/camera.png" width="50" height="50" id="logo" />
				<div id="title">PicSwap</div>
			</div>
            <div class="container">
				<nav class="nav navbar-nav navbar-right">
					<ul class="nav-class bar">
						<li>
							<form action="login.php" method="POST">
								<ul class="nav-class">
									<li><input type="text" name="LUname" placeholder="Username" required="required" /></li>
									<li><input type="password" name="LPword" placeholder="Password" required="required" /></li>
									<li><input type="submit" name="login" value="Login" class="Btn" /></li>
								</ul>
							</form>
						</li>
						
						<li>
							<div id="para" class="Btn">Signup</div>
						</li>
					</ul>
                    
                    
				</nav>
			</div>
        </div>
		
		
        <!--Signup Form-->
        <div class="signupForm">
			<div id="exit">close</div>
            <form action="signup.php" method="POST">
                <table class="signuptable">
                    <thead>
                        <tr><td colspan="2"><h3>Signup for Free!!!</h3></td></tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td><label>First Name</label></td>
                            <td><input type="text" name="fname" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Last Name</label></td>
                            <td><input type="text" name="lname" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Username</label></td>
                            <td><input type="text" name="uname" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Password</label></td>
                            <td><input type="password" name="pword" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Confirm Password</label></td>
                            <td><input type="password" name="pword2" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Date of Birth</label></td>
                            <td><input type="date" name="dob" required="required" /></td>
                        </tr>
                        <tr>
                            <td><label>Email</label></td>
                            <td><input type="email" name="email" required="required" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input class="Btn" type="submit" name="signup" />
                                <input class="Btn" type="reset" name="reset" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
		
		
		
		<!--scripts-->
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/JQ.js"></script>
    </body>
</html>