$(document).ready(function(){
    $('#para').click(function(){
            $('.signupForm').css({'margin-top': '3em', 'opacity': '1'});
        });
    $('#exit').click(function(){
            $('.signupForm').css({'margin-top': '0em', 'opacity': '0'});
            
        });
    
    //nav links --start--
    $('#1').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/home_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/home.png').fadeIn();
        });
    $('#2').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/message_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/message.png').fadeIn();
        });
    $('#3').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/friendrequest_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/friendrequest.png').fadeIn();
        });
    $('#4').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/upload_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/upload.png').fadeIn();
        });
    $('#5').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/favorites_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/favorites.png').fadeIn();
        });
    $('#6').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/settings_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/settings.png').fadeIn();
        });
    $('#7').mouseover(function(){
            $(this).attr('src', 'css/logos/icons/logout_r.png').fadeIn();
        }).mouseout(function(){
            $(this).attr('src', 'css/logos/icons/logout.png').fadeIn();
        });
    //nav links --end--
    
    $('#pro').click(function(){
            $('#content').attr('src', 'profileForm.php');
            $('#content').css({'background': 'rgba(0,0,0,.7)', 'opacity': '1'});
            $.post('scripts/pro.php');            
        });
    
    $('#acc').click(function(){
            $('#content').attr('src', 'accountForm.php');
            $('#content').css({'background': 'rgba(0,0,0,.7)', 'opacity': '1'});
             $.post('scripts/acc.php');   
        });
    $('#sec').click(function(){
            $('#content').attr('src', 'securityForm.php');
            $('#content').css({'background': 'rgba(0,0,0,.7)', 'opacity': '1'});
        });
    $('#priv').click(function(){
            $('#content').attr('src', 'privacyForm.php');
            $('#content').css({'background': 'rgba(0,0,0,.7)', 'opacity': '1'});
        });
});