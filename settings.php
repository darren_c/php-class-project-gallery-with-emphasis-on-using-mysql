<?php
    require 'connection.php';
	
	require 'login.php';
	//$user_id = mysql_result($query_run, 0, 'id');
	if(!logged_in())
	{
		header('Location: index.php');
	}
?>

<html>
    <head>
        <title>Welcome to Nocturnal</title>
        <link rel="icon" type="image/ico" href="css/logos/favicon.ico" />
        <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="css/settings.css" />   
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <script>
        	function starter() {
        		$.post('scripts/retrievePP.php',function(data){
        		var x = data.substring(3);
        		$('#profile').attr("src",x);
	        	});
        	}
        	$( document ).ready(function() {
    		starter();
			});
        </script>

    </head>
    
    <body>
        <div class="header" id="top">
			<div>
				<img src="css/logos/camera.png" width="50" height="50" id="logo" />
				<div id="title">PicSwap</div>
			</div>
            <div class="container">
				<nav class="nav navbar-nav navbar-right">
					<ul class="nav-class bar">
						<li>
                            <ul class="nav-class">
                                <li><p id="welcome">Welcome, <span><?php echo $_SESSION['user_id']." ".$_SESSION['username']; ?></span></p></li>
								<li><a href="account.php" title="Home"><img id="1" src="css/logos/icons/home.png" width="32" height="32" /></a></li>
                                <li><a href="" title="Messages"><img id="2" src="css/logos/icons/message.png" width="32" height="32" /></a></li>
                                <li><a href="" title="Friends"><img id="3" src="css/logos/icons/friendrequest.png" width="32" height="32" /></a></li>
                                <li><a href="" title="Upload"><img id="4" src="css/logos/icons/upload.png" width="32" height="32" /></a></li>
                                <li><a href="" title="Favorites"><img id="5" src="css/logos/icons/favorites.png" width="32" height="32" /></a></li>
                                <li><a href="settings.php" title="Settings"><img id="6" src="css/logos/icons/settings.png" width="32" height="32" /></a></li>
								<li><a href="logout.php" title="Logout"><img id="7" src="css/logos/icons/logout.png" width="32" height="32" /></a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
        </div>
        
        <div id="body_container">
            <div class="body_divs" id="left">
                <img id="profile" src="css/images/placeholder.jpg" height="256" width="256" />
                <br />
                <ul>
                    <li id="pro"><img src="css/logos/icons/profile.jpg" width="32" height="32" /><span>Profile</span></li>
                    <li id="acc"><img src="css/logos/icons/account.png" width="32" height="32" /><span>Account</span></li>
                    <li id="sec"><img src="css/logos/icons/padlock.png" width="32" height="32" /><span>Security</span></li>
                    <li id="priv"><img src="css/logos/icons/user-shield.png" width="32" height="32" /><span>Privacy</span></li>
                </ul>
            </div>
            
            <div class="body_divs" id="right">
                <div class="window" id="window">
					<iframe scrolling="no" id="content" name="content" src="" onLoad="iframeLoaded();">
						
					</iframe>
				</div>
			</div>
		</div>
        
        
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/jq2.js"></script>
		<script type="text/javascript">
			function iframeLoaded() {
				var iFrameID = document.getElementById('content');
				if(iFrameID) {
					  // here you can make the height, I delete it first, then I make it again
					  iFrameID.height = "";
					  iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
				}   
			}
		</script>
    </body>
</html>