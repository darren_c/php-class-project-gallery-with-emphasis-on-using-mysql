CREATE DATABASE IF NOT EXISTS picswap;
USE picswap;
DROP TABLE IF EXISTS users;
create table users (
	id INT,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	email VARCHAR(50),
	username VARCHAR(50),
	password VARCHAR(50),
	DateOfBirth DATE
);
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (1, 'Christina', 'Ryan', 'cryan0@squarespace.com', 'cryan0', '7fh72b', '1945/06/27');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (2, 'Sarah', 'Schmidt', 'sschmidt1@netscape.com', 'sschmidt1', 'zjd2xsh', '1986/04/09');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (3, 'Betty', 'Howard', 'bhoward2@earthlink.net', 'bhoward2', 'cAsKEwCAa0', '1977/01/21');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (4, 'Lori', 'Diaz', 'ldiaz3@google.co.jp', 'ldiaz3', 'JVXRMjmb', '1946/09/03');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (5, 'Stephen', 'Perry', 'sperry4@friendfeed.com', 'sperry4', 'jE9FNyaJ', '1997/06/25');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (6, 'Walter', 'Bradley', 'wbradley5@abc.net.au', 'wbradley5', 's97YQGYf2', '1995/11/07');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (7, 'Jeffrey', 'Rose', 'jrose6@vinaora.com', 'jrose6', 'FDPW3X', '1952/02/19');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (8, 'Willie', 'Sanchez', 'wsanchez7@ameblo.jp', 'wsanchez7', 'xFfe2WppQu', '1977/03/30');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (9, 'Mary', 'Thomas', 'mthomas8@phoca.cz', 'mthomas8', 'iRr7h9fM', '1977/11/19');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (10, 'Rose', 'Wright', 'rwright9@fema.gov', 'rwright9', 'OpKEga', '1969/01/27');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (11, 'Gary', 'Frazier', 'gfraziera@netvibes.com', 'gfraziera', 'T1zePzPs1s', '1970/09/08');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (12, 'Marilyn', 'Parker', 'mparkerb@xinhuanet.com', 'mparkerb', '9j8cIJKzzIP', '1955/01/04');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (13, 'Anna', 'Palmer', 'apalmerc@usatoday.com', 'apalmerc', '7Sjg5wVsm', '1955/04/26');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (14, 'Ernest', 'Mendoza', 'emendozad@pinterest.com', 'emendozad', 'j3x0NvPabe', '1966/04/17');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (15, 'Katherine', 'Gonzales', 'kgonzalese@si.edu', 'kgonzalese', 'uG0ruCt', '1999/11/13');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (16, 'Cynthia', 'Oliver', 'coliverf@nydailynews.com', 'coliverf', 'lTa0KzM7D', '2002/02/22');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (17, 'Martin', 'Crawford', 'mcrawfordg@phoca.cz', 'mcrawfordg', 'Ah4TOfUnJJaQ', '1984/04/20');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (18, 'Ryan', 'Moore', 'rmooreh@dedecms.com', 'rmooreh', 'acXMvBgXK6', '1985/03/18');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (19, 'Jacqueline', 'Cook', 'jcooki@de.vu', 'jcooki', 'ktR62SG', '1978/09/20');
insert into users (id, first_name, last_name, email, username, password, DateOfBirth) values (20, 'Bonnie', 'Stone', 'bstonej@hao123.com', 'bstonej', 'hQcBPr0', '1997/10/28');

DROP TABLE IF EXISTS Post;
create table Post(
   PostID    INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,PostDesc  VARCHAR(50)
  ,PostDate  DATE 
  ,UserID    INTEGER  NOT NULL
  ,PictureID INTEGER  NOT NULL
,FOREIGN KEY (UserID) REFERENCES User(UserID)
ON UPDATE CASCADE ON DELETE RESTRICT
,FOREIGN KEY (PictureID) REFERENCES Picture(PictureID)
ON UPDATE CASCADE ON DELETE RESTRICT
);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,1,6);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,5,4);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,3,5);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,8,7);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,6,6);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,4,4);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,1,2);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,2,1);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,5,2);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,7,3);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,4,8);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,6,9);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,9,7);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,2,6);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,3,4);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,6,3);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,7,6);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,9,8);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,10,7);
INSERT INTO Post(PostDesc,PostDate,UserID,PictureID) VALUES (NULL,NULL,10,10);

DROP TABLE IF EXISTS Picture;
CREATE TABLE  Picture(
   PictureID INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,UserID    INTEGER  NOT NULL
  ,GalleryID INTEGER  NOT NULL
  ,PostID    INTEGER  NOT NULL
  ,Location  VARCHAR(1000)
,FOREIGN KEY (UserID) REFERENCES User(UserID)
ON UPDATE CASCADE ON DELETE RESTRICT
,FOREIGN KEY (GalleryID) REFERENCES Gallery(GalleryID)
ON UPDATE CASCADE ON DELETE RESTRICT
,FOREIGN KEY (PostID) REFERENCES Post(PostID)
ON UPDATE CASCADE ON DELETE RESTRICT
);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (2,3,5,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (3,12,4,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (5,6,15,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (4,7,7,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (10,10,9,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (9,3,18,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (3,6,17,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (1,15,8,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (1,13,7,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (2,8,20,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (6,1,19,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (4,11,2,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (5,19,15,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (8,16,1,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (9,13,1,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (10,5,12,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (2,4,17,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (3,14,16,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (9,2,15,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (7,9,2,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (1,6,1,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (3,15,7,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (6,18,13,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (1,14,4,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (8,5,14,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (8,13,9,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (10,12,8,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (6,16,5,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (4,17,3,NULL);
INSERT INTO Picture(UserID,GalleryID,PostID,Location) VALUES (9,20,4,NULL);

DROP TABLE IF EXISTS Gallery;
CREATE TABLE IF NOT EXISTS Gallery(
   GalleryID INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT
  ,UserID    INTEGER  NOT NULL
  ,ImageName VARCHAR(20)
  ,ImageDesc VARCHAR(50)
  ,PostID    INTEGER  NOT NULL
,FOREIGN KEY (UserID) REFERENCES User(UserID)
ON UPDATE CASCADE ON DELETE RESTRICT
,FOREIGN KEY (PostID) REFERENCES Post(PostID)
ON UPDATE CASCADE ON DELETE RESTRICT
);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (2,NULL,NULL,9);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (3,NULL,NULL,2);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (3,NULL,NULL,5);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (5,NULL,NULL,4);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (4,NULL,NULL,3);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (1,NULL,NULL,6);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (2,NULL,NULL,15);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (1,NULL,NULL,1);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (5,NULL,NULL,16);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (6,NULL,NULL,8);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (8,NULL,NULL,4);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (9,NULL,NULL,5);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (7,NULL,NULL,11);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (8,NULL,NULL,13);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (10,NULL,NULL,10);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (1,NULL,NULL,12);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (10,NULL,NULL,7);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (3,NULL,NULL,18);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (6,NULL,NULL,10);
INSERT INTO Gallery(UserID,ImageName,ImageDesc,PostID) VALUES (9,NULL,NULL,17);
