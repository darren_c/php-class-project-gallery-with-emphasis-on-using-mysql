<style type="text/css">
    .accountTable, .accountTable tr td
    {
        color: white;
        padding: .5em;
    }
    
    .accountTable tr td input[type=text],
    .accountTable tr td input[type=password],
    .accountTable tr td input[type=email],
    .accountTable tr td input[type=date], 
    .accountTable tr td textarea
    {
        border: solid 1px white;
        background: black;
        color: white;
        padding: 3px;
        margin-top: .5em;
        border-radius: 10px;
        margin-left: .5em;
        margin-right: .5em;
        transition: 500ms;
        text-indent: 10px;
    }
    
    .Btn
    {
        padding-left: 20px;
        padding-right: 20px;
        text-indent: 0px;
        padding: 4px;
        border: solid 1px white;
        background: transparent;
        color: white;
        width: 90px;
        cursor: pointer;
    }
    
    .Btn:hover
    {
        transition: 500ms;
        color: white;
    }
    
    textarea:focus, input:focus
    {
        outline: none;
        box-shadow: 0 0 10px white;
    }
</style>


<form action="changeSec.php" method="POST">
    <table class="accountTable">
        <thead>
            <tr><td colspan="2"><h3>Security Settings</h3></td></tr>
        </thead>
        
        <tbody>
            <tr>
                <td><label>Current Password</label></td>
                <td><input type="password" name="pword" required="required" /></td>
            </tr>
            <tr>
                <td><label>New Password</label></td>
                <td><input type="password" name="nPword" required="required" /></td>
            </tr>
            <tr>
                <td><label>Confirm Password</label></td>
                <td><input type="password" name="pword2" required="required" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input class="Btn" type="submit" name="submit" />
                    <input class="Btn" type="reset" name="reset" />
                </td>
            </tr>
        </tbody>
    </table>
</form>

